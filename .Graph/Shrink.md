# Shrink
Guide
https://pve.proxmox.com/wiki/Shrink_Qcow2_Disk_Files

- Option #1: Shrink your disk without compression (better performance, larger disk size):  
`qemu-img convert -O qcow2 image.qcow2_backup image.qcow2`

- Option #2: Shrink your disk with compression (smaller disk size, takes longer to shrink, performance impact on slower systems):  
`qemu-img convert -O qcow2 -c image.qcow2_backup image.qcow2`